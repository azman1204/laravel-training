<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\RequestController;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

Route::middleware(['audit.trail'])->group(function() {
    // show login page
    Route::view('/login', 'login');

    // handle login
    Route::post('/login', function(Request $req) {
        $user_id = $req->user_id;
        $password = $req->password;

        // auth
        $arr = [
            'user_id'  => $user_id, 
            'password' => $password
        ];

        if(auth()->attempt($arr)) {
            // user berjaya auth
            echo "Berjaya";
        } else {
            echo "Tidak berjaya";
        }
    });


    // generate hashed password
    Route::get('generate-pwd', function() {
        return Hash::make('1234');
    });

    // download file
    Route::get('/download-file/{id}', [RequestController::class, 'downloadFile']);

    // list all uploaded file
    Route::get('/uploaded-file', [RequestController::class, 'fileList']);

    // show upload form
    Route::get('/form-upload', [RequestController::class, 'uploadForm']);

    // handle uploaded file
    Route::post('/upload', [RequestController::class, 'saveFile']);

    Route::get('/redirect', [RequestController::class, 'redirect']);

    // Request / respones / Middleware
    Route::get('/download', [RequestController::class, 'download']);

    // chap 7 : Collecting and Handling User Data
    Route::get('/form1', function() {
        return view('form1');
    });

    Route::post('/form1', function(Request $req) {
        // var_dump($req->all());
        // var_dump($req->except('_token'));
        // var_dump($req->only(['nama']));
        // var_dump($req->method());
        // var_dump(($req->isMethod('get')));
        // var_dump($req->nama);
        // var_dump($req->input('nama'));
    
        // validation, jika berjaya proceed to save data, else redirect back
        $rules = [
            'nama'  => 'required|min:3|max:10',
            'email' => 'required|email|max:10'
        ];
    
        $message = [
            'nama.required'  => 'Nama wajib diisi',
            'email.required' => 'Emel wajib diisi',
            'email.email'    => 'Emel mestilah format emel, i.e abc@def.com',
            'email.max'      => 'Emel mestilah kurang dari 10 karakter'
        ];
    
        // $req->validate($rules);
        // custom message
        $req->validate($rules, $message);
    
        $pengguna = new Pengguna();
        $pengguna->nama  = $req->nama;
        $pengguna->email = $req->email;
        $pengguna->save();
        echo "data telah disimpan";
    });

    // Blade Template
    Route::get('/blade2', function() {
        return view('dashboard');
    });

    Route::get('/home', function() {
        return view('home');
    });
});


Route::get('/blade1', function() {
    $arr = \App\Models\Pengguna::all();
    return view('blade1')
    ->with('pengguna', $arr)
    ->with('alamat', '<b>Bangi, Selangor</b>')
    ->with('nama', 'John Doe'); // blade1.blade.php
});



// Query Builder

// laravel_training.test/query1/2
Route::get('/query1/{id}', function($id) {
    $rs = DB::select('SELECT * FROM pengguna WHERE id = ?', [$id]);
    //var_dump($rs);
    foreach($rs as $pengguna) {
        //echo $pengguna->nama . '<br>';
        echo "nama saya {$pengguna->nama} <br>";
    }
});

// laravel_training.test/insert/hassan/hassan@gmail.com
Route::get('/insert/{nama}/{email}', function($nama, $email) {
    //DB::insert("INSERT INTO pengguna(nama, email) VALUES(?, ?)", ['Aminah', 'aminah@gmail.com']);
    DB::insert("INSERT INTO pengguna(nama, email) VALUES(?, ?)", [$nama, $email]);
});

Route::get('/update-pengguna', function() {
    DB::update("UPDATE pengguna SET email = ? WHERE id = ?" ,['hassan@yahoo.com', 4]);
});

Route::get('/delete-pengguna/{id}', function($id) {
    DB::delete("DELETE FROM pengguna WHERE id = ?", [$id]);
});

// Eloquent
Route::get('/query2', function() {
    $rs = \App\Models\Pengguna::all();
    foreach($rs as $pengguna) {
        echo "Nama pengguna ialah {$pengguna->nama} <br>";
    }
});

// insert
Route::get('/insert2', function() {
    $pengguna = new \App\Models\Pengguna();
    $pengguna->nama = 'Ali';
    $pengguna->email = 'ali@gmail.com';
    $pengguna->save();
});

// update
Route::get('/update-pengguna2', function() {
    $pengguna = \App\Models\Pengguna::find(1); // find() cari by primary key, return an object
    $pengguna->email = 'john.doe@yahoo.com';
    $pengguna->save();
});

// delete
Route::get('/delete-pengguna2', function() {
    $pengguna = \App\Models\Pengguna::find(1);
    $pengguna->delete();
});




Route::get('/pengguna', function() {
    return \App\Models\Pengguna::all();
});

Route::get('/pengguna/listing', [PenggunaController::class, 'listing']);

Route::get('/task/list', [Taskcontroller::class, 'index']);
Route::post('/task/store', [TaskController::class, 'store']);

// laravel_training.test/welcome
// error. hanya laravel 7 dan ke bawah
//Route::get('/welcome', 'WelcomeController@index');

// ok. laravel 8
Route::get('/welcome', [WelcomeController::class, 'index']);


// see page 29
// laravel_training.test/users/100/friends. {..} = dynamic data
Route::get('/users/{id}/friend', function($id) {
    return "Hello $id";
});

// laravel_training.test/users/123/edit. ? = optional
Route::get('/users/{id?}/edit', function($id='hello') {
    return "Hello $id";
});




// laravel_training.test. callback function
// param1 = pattern URL, param2 = func. , etc.
Route::get('/', function () {
    return view('welcome');
});

// laravel_training.test/hello
Route::get('/hello', function() {
    return view('hello'); // resources/views/hello.blade.php
});

// laravel_training.test/hello-world
Route::get('/hello-world', function() {
    return "<h4>Hello World</h4>";
});
