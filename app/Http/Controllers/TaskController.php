<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class TaskController extends Controller {
    // ../task/store
    public function store(Request $req) {
        echo $req->nama_buah;
    }

    // ../task/list. index = special name, default function
    public function index() {
        $fruits = ['Jambu', 'Rambutan', 'Mangga'];
        return view('task.index')->with('buah', $fruits); // with() = hantar data ke view
        // resources/views/task/index.blade.php
    }
}
