<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Upload;
class RequestController extends Controller {
    public function download() {
        // default download dari /public folder
        return response()->download('test.csv', 'nama_baru.csv');
    }

    public function redirect() {
        return redirect('/home');
    }

    public function uploadForm() {
        return view('request.form');
    }

    public function saveFile(Request $req) {
        // validation
        $rules = [
            'doc' => 'required|mimes:pdf,png|max:100'
        ];
        $message = [];
        $req->validate($rules, $message);

        // save file
        // Storage::put('')
        $ori_name = $req->file('doc')->getClientOriginalName();
        $ext = $req->file('doc')->getClientOriginalExtension(); // pdf, docx,...
        $mime = $req->file('doc')->getMimeType();
        $new_name = time() . ".$ext";
        $size = $req->file('doc')->getSize();
        $req->file('doc')->storeAs('upload', $new_name);

        // save meta data
        $upload = new Upload();
        $upload->ori_name = $ori_name;
        $upload->new_name = $new_name;
        $upload->file_type = $mime; // mime type
        $upload->file_size = $size;
        $upload->save();
        // return 'File has been uploaded';
        return redirect('/uploaded-file')->with('msg', 'File has been uploaded');
    }

    // download file
    public function downloadFile($id) {
        $file = Upload::find($id);
        $file_name = 'upload/' . $file->new_name;
        //echo $file_name;
        $f = \Illuminate\Support\Facades\Storage::get($file_name);
        return (new Response($f, 200))
        ->header('Content-Type', $file->file_type)
        ->header('Content-Disposition', "attachment; filename=" .  $file->ori_name);
    }


    // list uploaded files
    public function fileList() {
        $files = Upload::all();
        return view('request.list')->with('files', $files);
    }
}
