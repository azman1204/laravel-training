<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Http\Request;
use App\Models\AuditTrail as At;

class AuditTrail {
    public function handle(Request $request, Closure $next) {
        $path = $request->path();
        $ip = $request->ip();
        $user_id = 'anonymous';
        $agent = $request->server('HTTP_USER_AGENT'); // browser
        $referer = $request->server('HTTP_REFERER'); // browser
        $method = $request->method();
        $get_data = json_encode($request->all());

        $at = new At();
        $at->path = $path;
        $at->ip = $ip;
        $at->user_id = $user_id;
        $at->get_data = $get_data;
        $at->method = $method;
        $at->agent = $agent;
        $at->referer = $referer; // prev page
        $at->save();

        return $next($request);
    }
}
