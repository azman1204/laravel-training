<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Model
{
    use HasFactory;

    public $table = 'pengguna'; // config
    public $timestamps = false; // dalam table pengguna tiada field created_at dan updated_at
}
