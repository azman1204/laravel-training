@extends('layout.master')
@section('content')


@if(session()->has('msg'))
    <div class="alert alert-success">{{ session('msg') }}</div>
@endif

<a href="/form-upload" class="btn btn-primary btn-sm">New Upload</a>

<table class="table table-bordered table-striped">
    <tr>
        <td>#</td>
        <td>File Name</td>
        <td>Size (Kb)</td>
        <td>Type</td>
        <td>Upload Date</td>
        <td>Action</td>
    </tr>
    @foreach($files as $f)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $f->ori_name }}</td>
            <td>{{ $f->file_size }}</td>
            <td>{{ $f->file_type }}</td>
            <td>{{ $f->created_at }}</td>
            <td><a href="/download-file/{{ $f->id }}" class="btn btn-info btn-sm">Download</a></td>
        </tr>
    @endforeach
</table>
@endsection