@extends('layout.master')
@section('content')

@if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $err)
            <li>{{ $err }}</li>
        @endforeach
    </ul>
@endif

<form method="POST" action="/upload" enctype="multipart/form-data">
    @csrf
    File : <input type="file" name="doc">
    <input type="submit" value="Upload" class="btn btn-primary">
</form>

@endsection