@extends('layout.master')

@section('title', 'Dashboard')

@section('content')
    Welcome to your application dashboard!
@endsection

@section('footerScript')
    @parent
    <script src='dashboard.js'></script>
@endsection