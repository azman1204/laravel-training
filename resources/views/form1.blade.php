@extends('layout.master')
@section('content')

@if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $err)
            <li>{{ $err }}</li>
        @endforeach
    </ul>
@endif

<form action="/form1" method="post">
    @csrf
    Nama : <input type="text" name="nama" value="{{ old('nama') }}" class="form-control">
    <br>
    Emel : <input type="email" name="email" value="{{ old('email') }}" class="form-control">
    <br>
    <input type="submit" value="Simpan" class="btn btn-primary">
</form>

@endsection